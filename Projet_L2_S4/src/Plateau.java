
/**
 * Classe représentant un monde et gérant son évolution
 * 
 * @author LECOMTE Rodolphe
 *
 */
public class Plateau {
	private ListeTrie<Generation> tlt;
	private int[] survie;
	private int[] naissance;
	private boolean estCirculaire;
	private boolean estFerme;
	private int[] limites;
	private String type;
	private int deplacementx;
	private int deplacementy;
	private int periode;
	private int debut;

	/**
	 * Créer une un nouveau monde
	 * 
	 * @param s
	 *            Le contenu du fichier LIF
	 * @param Survie
	 *            Un tableau qui contient les régles de survie des cellules de
	 *            ce monde
	 * @param Naissance
	 *            Un tableau qui contient les régles de naissance des cellules
	 *            de ce monde
	 * @param typeMonde
	 *            Le type de monde 0 pour le monde normal 1 pour le monde limité
	 *            8 pour le monde circulaire
	 * @param Limites
	 *            Les limites de ce monde dans le cas ou il est limité ou
	 *            circulaire
	 */
	public Plateau(String s, int[] Survie, int[] Naissance, int typeMonde, int[] Limites) {
		this.survie = Survie;
		this.naissance = Naissance;
		String s2[] = s.split("\n");
		int positionx = 0;
		int positiony = 0;
		if (typeMonde == 0) {
			this.estCirculaire = false;
			this.estFerme = false;
			this.limites = new int[] { 0, 0, 0, 0 };
		} else if (typeMonde == 1) {
			this.estCirculaire = false;
			this.estFerme = true;
			this.limites = Limites;
		} else if (typeMonde == 8) {
			this.estCirculaire = true;
			this.estFerme = false;
			this.limites = Limites;
		}
		ListeTrie<Cellule> lt = new ListeTrie<Cellule>();
		for (String s3 : s2) {
			if (s3.matches("#P\\s-?\\d\\s-?\\d$")) {
				s3 = s3.substring(3);
				String s4[] = s3.split(" ");
				positionx = Integer.parseInt(s4[0]);
				positiony = Integer.parseInt(s4[1]);
			} else if (s3.matches("[.*\\**]+")) {
				int tempPosX = positionx;
				char c[] = s3.toCharArray();
				for (char c1 : c) {
					if (c1 == '.') {
						tempPosX++;
					} else if (c1 == '*') {
						lt.add(new Cellule(positiony, tempPosX));
						tempPosX++;
					}
				}
				positiony++;
			}
		}
		this.tlt = new ListeTrie<Generation>();
		this.tlt.add(new Generation(lt, 0));
	}

	/**
	 * Renvoie une chaîne de caractére qui représente la génération en 2
	 * dimensions
	 * 
	 * @param lt
	 *            La liste de la génération que l'on veut afficher
	 * @return Une chaîne de caractére qui représente la génération en 2
	 *         dimensions
	 */
	public String printGeneration(ListeTrie<Cellule> lt) {
		int[] border = this.getBorder(lt);
		String s = border[0] + "," + border[1] + "," + border[2] + "," + border[3] + "\n";
		Cellule c = lt.getPremier();
		for (int i = border[0]; i <= border[1]; i++) {
			s = s + i;
			if (i < 0) {
				s = s + "  |";
			} else {
				s = s + "   |";
			}
			for (int j = border[2]; j <= border[3]; j++) {
				if (c != null && c.getPosx() == i && c.getPosy() == j) {
					s = s + "O ";
					lt = lt.queue();
					c = lt.getPremier();
				} else {
					s = s + "  ";
				}
			}
			s = s + "|\n";
		}
		return s + "\n";
	}

	/**
	 * Renvoie une chaîne de caractére qui représente la derniére génération de
	 * ce monde en 2 dimensions
	 * 
	 * @return Une chaîne de caractére qui représente la derniére génération en
	 *         2 dimensions
	 */
	public String printLastGeneration() {
		return this.printGeneration(this.tlt.getDernier().getLt());
	}

	/**
	 * Prend la génération actuelle et créer la génération suivante puis
	 * l'ajoute au monde
	 * 
	 * @return Renvoie la génération créée par cette fonction
	 */
	public Generation creerGenerationSuivante() {
		Generation g = this.tlt.getDernier().nextGeneration(this.survie, this.naissance, this.estCirculaire,
				this.estFerme, this.limites);
		this.tlt.add(g);
		return g;
	}

	/**
	 * Créer toutes les générations de 0 a N sauf si elles existent déja
	 * 
	 * @param n
	 *            La génération a créer
	 */
	public void creerGenerationN(int n) {
		for (int i = this.tlt.getDernier().getNum(); i < n; i++) {
			this.creerGenerationSuivante();
		}
	}

	/**
	 * Renvoie si le monde est circulaire
	 * 
	 * @return Renvoie la variable estCirculaire
	 */
	public boolean isEstCirculaire() {
		return estCirculaire;
	}

	/**
	 * Renvoie si le monde est ferme
	 * 
	 * @return Renvoie la variable estFerme
	 */
	public boolean isEstFerme() {
		return estFerme;
	}

	/**
	 * Renvoie un tableau contenant les limites de ce monde
	 * 
	 * @return Le tableau des limites du monde (ymin ymax xmin xmax)
	 */
	public int[] getLimites() {
		return limites;
	}

	/**
	 * Renvoie une chaîne contenant le résultat de print génération pour toutes
	 * les générations créées pour ce monde
	 * 
	 * @return Une chaîne contenant le résultat de print génération pour toutes
	 *         les générations créées pour ce monde
	 */
	public String touteLesGeneration() {
		String s = "";
		ListeTrie<Generation> lt = this.tlt.clone();
		while (!lt.estVide()) {
			s = s + this.printGeneration(lt.pop().getLt());
		}
		return s;
	}

	/**
	 * Test si le monde est mort pour la dernière génération
	 * 
	 * @return Renvoie true si la derniére génération est morte et false sinon
	 */
	public boolean estMort() {
		return tlt.getDernier().getLt().estVide();
	}

	/**
	 * Renvoie un tableau d'entier qui contient dans l'ordre la plus petite
	 * position de y, sa plus grande, la plus petite position de x et sa plus
	 * grande
	 * 
	 * @param lt
	 *            La liste dont on veut les maximum et minimum
	 * @return Un tableau d'entier qui contient dans l'ordre la plus petite
	 *         position de y, sa plus grande, la plus petite position de x et sa
	 *         plus grande
	 */
	public int[] getBorder(ListeTrie<Cellule> lt) {
		if (this.estFerme || this.estCirculaire) {
			return this.limites;
		} else {
			int[] border = { lt.getPremier().getPosx(), lt.getPremier().getPosx(), lt.getPremier().getPosy(),
					lt.getPremier().getPosy() };
			while (!lt.estVide()) {
				Cellule c = lt.getPremier();
				if (border[1] < c.getPosx()) {
					border[1] = c.getPosx();
				}
				if (border[2] > c.getPosy()) {
					border[2] = c.getPosy();
				}
				if (border[3] < c.getPosy()) {
					border[3] = c.getPosy();
				}
				lt = lt.queue();
			}
			return border;
		}
	}

	/**
	 * Cherche la première génération contenant un motif donné
	 * 
	 * @param lt
	 *            La liste des générations composant le motif
	 */
	public void getFirstGen(ListeTrie<Generation> lt) {
		ListeTrie<Generation> lt1 = this.tlt.clone();
		ListeTrie<Generation> lt2 = lt.clone();
		while (!lt1.estVide()) {
			while (!lt2.estVide()) {
				if (lt1.getPremier().toZeroListe().equals(lt2.getPremier().toZeroListe())) {
					this.debut = lt1.getPremier().getNum();
					return;
				}
				lt2 = lt2.queue();
			}
			lt1 = lt1.queue();
			lt2 = lt.clone();
		}

	}

	/**
	 * Cherche le type du motif créer par les Cellules
	 * 
	 * @param numMax
	 *            Le nombre maximal de génération pour chercher le type
	 * @return Une chaîne décrivant le type de motif formé par ce monde
	 */
	public String getType(int numMax) {
		type = "inconnu";
		this.creerGenerationN(numMax);
		ListeTrie<Generation> lt1 = this.tlt.clone();
		ListeTrie<Generation> lt2 = this.tlt.clone().queue();
		periode = 0;
		while (!lt1.estVide() && lt1.getPremier().getNum() < numMax) {
			while (!lt2.estVide() && lt2.getPremier().getNum() < numMax) {

				if (lt1.getPremier().estMort()) {// Gestion monde mort
					type = "mort";
					this.debut = lt1.getPremier().getNum();
					return this.type + " Debut:" + this.debut;
				} else if ((!type.equals("mort")) && lt1.getPremier().compareGeneration(lt2.getPremier())) {
					// gestion stable et oscillateur
					ListeTrie<Generation> ltSchema = lt1.clone();
					while (!ltSchema.equals(lt2)) {
						if (ltSchema.getPremier().compareGeneration(lt2.getPremier())) {
							lt1 = ltSchema;
						}
						ltSchema = ltSchema.queue();
					}
					periode = lt2.getPremier().getNum() - lt1.getPremier().getNum();
					ltSchema = new ListeTrie<Generation>();
					while (!lt1.equals(lt2)) {
						ltSchema.add(lt1.pop());
					}
					this.getFirstGen(ltSchema);
					if (periode == 1) {// Si la periode a une valeure de 1 c'est
										// un monde
										// stable
						type = "stable";
						return this.type + " Debut:" + this.debut;
					} else {// Si la période est supérieure a 1 c'est un
							// oscillateur
						type = "oscillateur";
						return this.type + " periode:" + this.periode + " Debut:" + this.debut;
					}

				} else if ((!type.equals("stable") && !type.equals("oscillateur"))
						&& (lt1.getPremier().toZeroListe().equals(lt2.getPremier().toZeroListe()))) {// Gestion

					this.deplacementx = lt2.getPremier().getLt().getPremier().getPosx()
							- lt1.getPremier().getLt().getPremier().getPosx();
					this.deplacementy = lt2.getPremier().getLt().getPremier().getPosy()
							- lt1.getPremier().getLt().getPremier().getPosy(); // vaisseau

					ListeTrie<Generation> ltSchema = lt1.clone();
					while (!ltSchema.equals(lt2)) {
						if (ltSchema.getPremier().compareGeneration(lt2.getPremier())) {
							lt1 = ltSchema;
						}
						ltSchema = ltSchema.queue();
					}
					periode = lt2.getPremier().getNum() - lt1.getPremier().getNum();
					ltSchema = new ListeTrie<Generation>();
					while (!lt1.equals(lt2)) {
						ltSchema.add(lt1.pop());
					}
					this.getFirstGen(ltSchema);
					type = "Vaisseau";
					return this.type + " periode:" + this.periode + " deplacement (x,y):(" + this.deplacementx + ","
							+ this.deplacementy + ") Debut:" + this.debut;
				} else {
				}
				lt1 = lt1.queue();
				lt2 = lt2.queue().queue();
			}
			lt1 = lt1.queue();
			lt2 = lt1.clone();
			lt2 = lt2.queue();
		}
		return this.type;
	}

}
