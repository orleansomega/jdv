import java.io.*;
import javax.swing.Timer;
import java.awt.event.*;

/**
 * Classe gérant l'affichage temporel ainsi que d'autre fonctions
 * 
 * @author NAIL Theo
 *
 */
public class Core {
	/**
	 * Permet de faire le rendu du programme 
	 * @param p
	 * 			Le monde que l'on souhaite afficher 
	 * @param num
	 * 			Le nombre de génération a afficher
	 */
	public static void Render(Plateau p, int num) {

		Timer t = new Timer(1000, new ActionListener() {
			int i = 0;

			public void actionPerformed(ActionEvent e) {
				if (i <= num) {
					System.out.println(p.printLastGeneration());
					p.creerGenerationSuivante();
				}
				i++;

			}
		});

		t.start();
		try {
			System.in.read();
		} catch (IOException e) {
		}
		t.stop();
	}

	/**
	 * Test si la chaîne est un entier
	 * @param s
	 * 			La chaîne a tester
	 * @return
	 * 			Renvoie true si la chaîne est un entier et false sinon
	 */
	public static boolean isInt(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * Renvoie un tableau de fichier contenu dans le dossier passé en paramètre
	 * @param arg
	 * 			Le dossier a parcourir
	 * @return
	 * 			Un tableau de fichier
	 */
	public static File[] FolderArr(String arg) {
		File folder = new File(arg);
		File[] res;
		res = folder.listFiles();
		return res;
	}

}